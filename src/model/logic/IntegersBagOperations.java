package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	
	//operacion 1
	public int getTotal (IntegersBag bag){
		int total = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				total += iter.next();
			}
		}			
		return total;
	}
	
	//operacion 2
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if (bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext()){
				value= iter.next();
				if(value<min){
					min = value;
				}
			}
		}
		return min;
	}
	
	//operacion 3
	public int evenCount(IntegersBag bag){
		int count = 0;
		int value;
		
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if((value % 2)==0){
					count++;
				}
			}
		}
		return count;
	}
}
